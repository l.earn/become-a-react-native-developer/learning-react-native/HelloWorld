import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {name as appName, user} from './app.json';

export default class HelloWorld extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Hello World, {user}!
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DEAD',
    },
    welcome: {
        fontSize: 42,
        textAlign: 'center',
        margin: 10,
    },
});

AppRegistry.registerComponent(appName, () => HelloWorld);